# GIANT.jl

GIANT Is An NMF Toolbox.

GIANT is a collection of algorithms for nonnegative matrix factorization (NMF), written in pure Julia.
It aims at providing fast and easy-to-use implementations of the most popular NMF algorithms.

GIANT is free software, licensed under the [GNU GPL v3](http://www.gnu.org/licenses/gpl.html).

## Problem statement and notations

NMF consists in the following optimization problem.
Given a data matrix  $`X \in \mathbb{R}^{m \times n}`$ and a factorization rank $`r`$, compute the factors $`W \in \mathbb{R}^{m \times r}_{+}`$ and $`H \in \mathbb{R}^{r \times n}_{+}`$ such that $`X \approx WH`$.
In general, this reduces to
$`\min\limits_{W \geq 0, W \in \Omega_{W}, H \geq 0, H \in \Omega_{H}}  d( X, WH )`$ where $`d`$ is some data fidelity measure between $X$ and its low-rank approximation, $WH$, chosen as objective function and $`\Omega_{W}, \Omega_{H}`$ are some constraint sets.

Notations in the NMF literature vary a lot.
In GIANT, we try to stick to the notation above, also used in [the NMF book](https://sites.google.com/site/nicolasgillis/book).

## TODO

Algorithms:
- [x] NNLS with active-set algorithm
- [x] Basic HALS
- [ ] Accelerated HALS
- [x] k-sparse NNLS
- [ ] Sparse NMF (L1 and L0)
- [x] Separable NMF (SPA, RandSPA, VCA, SNPA)
- [x] NMF with Beta-divergence via MU
- [ ] Extrapolated MU 
- [ ] Minimum-volume NMF
- [ ] Symmetric NMF

Utilities:
- [x] Hyperspectral tools
- [ ] Topic modeling tools

## Algorithms

### HALS

todo

### Active-set

todo

### Separable NMF

todo

### NMF with beta-divergence

A multiplicative updates (MU) algorithm to solve NMF using β-divergence as an objective function. 
β-divergence is defined as follows, for a given parameter β: <br>
$`d_\beta(x \mid y)= \begin{cases}\frac{1}{\beta(\beta-1)}\left(x^\beta+(\beta-1) y^\beta-\beta x y^{\beta-1}\right) & \text { for } \beta \in \mathbb{R} \backslash\{0,1\} \\ x \log \frac{x}{y}-x+y & \text { for } \beta=1 \\ \frac{x}{y}-\log \frac{x}{y}-1 & \text { for } \beta=0\end{cases}`$

<details>
  <summary markdown="span">Click for details</summary>

Reference:
> Leplat, V., Gillis, N., & Idier, J. (2021). Multiplicative updates for NMF with β-divergences under disjoint equality constraints. SIAM Journal on Matrix Analysis and Applications, 42(2), 730-752.

Present implementation by Timothy Baeckelant.

Test: <br>
`julia> include("test/test_betanmf.jl")`
</details>

## Utilities

### Hyperspectral unmixing

The function `displayabundancemap` in `hyperspectral.jl` displays the mixing matrix resulting from the unmixing of an hyperspectral image.

Below is an example of a complete hyperspectral unmixing task. 
In this example the input data is the HSI Jasper, stored under variable `Y` in a Matlab-format file `jasper.mat`, so we need an extra package (`add MAT`) to open it.

``` julia
using MAT
data = matopen("path/to/data/jasper.mat");
M = Array(read(data, "Y"));
using GIANT
W, H = nmf(M, 4);
displayabundancemap(H, 100, 100, "abundancemap.png", bw=false)
```

`


### TODO
