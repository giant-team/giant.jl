using GIANT
using LinearAlgebra

# Test k-sparse NNLS, min_x ||Ax-b|| s.t. ||x||_0 <= k
# Parameters
m = 12
r = 8
k = 4
targetk = 4
addnoise = true

trueA = rand(m, r)
truex = rand_ksparse_vect(r, k)
trueb = trueA * truex

if addnoise
    noiseb = randn(m)
    noiseb ./= norm(noiseb)
    noiseb .*= norm(trueb) * 0.01
    trueb = trueb + noiseb
    trueb[trueb.<0] .= 0 # ensure nonnegativity
end

AtA = trueA'*trueA
Atb = trueA'*trueb
btb = trueb'*trueb

println("Testing arborescent for k-sparse NNLS")
arbox = arborescent(AtA, Atb, btb, targetk)
display(hcat(truex, arbox))

println("\nTesting arborescent for biobjective k-sparse NNLS")
front = arborescent(AtA, Atb, btb, 1, returnpareto=true)
display(front)

println("\nTesting arborescent with nbnodes")
arbox, nbnodes = arborescent(AtA, Atb, btb, targetk, returnnbnodes=true)
display(arbox)
println("$(nbnodes) nodes explored")

# For matrix
# # Parameters
# m = 100
# n = 1000
# r = 20
# k = 10
# addnoise = false

# realW = rand(m, r)
# realH = rand_ksparse(r, n, k)

# # Normalize cols of H to unit l1-norm (to test sum-to-one constraint)
# # for col in eachcol(realH)
# #     col ./= sum(col)
# # end

# X = realW * realH


# # Add noise to M
# if addnoise
#     noiseX = randn(m, n)
#     noiseX ./= norm(noiseX)
#     noiseX .*= norm(X) * 0.01
#     X .+= noiseX
#     X[X.<0] .= 0 # Nonnegativity
# end

# println("Real H")
# display(realH)

# println("\nCompute H with bruteforce")
# bfH = rand(r, n)
# @time bfnbnodes = ksparse_updtH!(X, copy(realW), bfH, k, solver=bruteforce)
# println("Error = $(norm(realH - bfH))")
# println("$(bfnbnodes) nodes explored.")
# display(bfH)

# println("\nCompute H with arborescent")
# arboH = rand(r, n)
# @time arbonbnodes = ksparse_updtH!(X, copy(realW), arboH, k)
# println("Error = $(norm(realH - arboH))")
# println("$(arbonbnodes) nodes explored.")
# display(arboH)

# println("\nCompute H with arborescent sum-to-one")
# arboS1H = rand(r, n)
# @time arboS1nbnodes = ksparse_updtH!(copy(M), copy(realW), arboS1H, k, sumtoone=true)
# println("Error = $(norm(realH - arboS1H))")
# println("$(arboS1nbnodes) nodes explored.")
# # display(arboH)
