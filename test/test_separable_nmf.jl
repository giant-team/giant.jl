using GIANT
using LinearAlgebra
using Random

# Data parameters
m = 200
n = 1000
r = 10
nl = 0.1 # noise level

# randSPA parameters
Prank = 0 # if Prank=0, use default from randSPA
κ = 1.5

# Generate data
realW = rand(m, r)
realK = randperm(n)[1:r]
realH = rand(r, n)
realH[:, realK] .= Matrix(I, r, r)

# Normalize
for col in eachcol(realW)
    col ./= sum(col)
end
for col in eachcol(realH)
    col ./= sum(col)
end

# Compute X
X = realW * realH

# Add noise
noise = randn(m, n)
X .+= nl * noise / norm(noise) * norm(X)

# Try to find W with SPA
println("Running SPA")
@time spaK = spa(X, r)

# Try to find W with RandSPA
println("Running RandSPA")
g = x -> randorth!(x, κ)
randspa(X, r, Prank = Prank, genP = g)
@time randspaK = randspa(X, r, Prank = Prank, genP = g)

# Try to find W with VCA
println("Running VCA")
@time vcaK = vca(X, r)

# Try to find W with SNPA
println("Running SNPA")
@time snpaK = snpa(X, r, normalize=true)

display(hcat(sort(realK), sort(spaK), sort(randspaK), sort(vcaK), sort(snpaK)))
# display(hcat(sort(realK), sort(snpaK)))
