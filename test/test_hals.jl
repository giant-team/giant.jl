using GIANT
using LinearAlgebra
using Plots

# Dimensions of test data
m = 20
r = 6
n = 200

# Generate random synthetic data
trueW = rand(m,r)
trueH = rand(r,n)
X = trueW * trueH

# Solve NMF
@time W, H, t, e = nmf(X, r, benchmark=true)

println("Final relative error: $(fronorm(X, W, H')/norm(X, 2))")

plot(t, e)
