using GIANT
using LinearAlgebra
using Plots

# Dimensions of test data
m = 10
r = 6
n = 15

# Generate random synthetic data
trueW = rand(m,r)
trueH = rand(r,n)
X = trueW * trueH

# Solve NMF with beta-divergence
beta = -1.0
W, H, t, e = nmf(X, r, maxiter=100, updaterW=beta_updtW, updaterH=beta_updtH,
                 objfunction=betadiv, benchmark=true, beta=beta)

# Display results
plot(t, e, label="beta = "*string(beta), yaxis=:log, linewidth=2, show=true,
     xlabel="Time (s)", ylabel="Beta-divergence")
println(string(t[end]))
println(string(e[end]))
