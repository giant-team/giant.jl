using GIANT
using LinearAlgebra

m = 10
r = 6
n = 15

############################################################
println("Test activeset")

realx = rand(r)
A = rand(m, r)
b = A * realx

@time myx1 = activeset(A, b)
@time myx2 = activeset(A, b, rand(r))

AtA = A'*A
Atb = A'*b
@time myx3 = activeset(AtA, Atb, inputgram=true)

@time myx4 = activeset(A, b, sumtoone=true)

btb = b'*b
@time myx5, err = activeset(AtA, Atb, inputgram=true, outerror=true, btb=btb)

# This should give error
# @time myx6 = activeset(AtA, Atb, inputgram=true, outerror=true)

display(hcat(realx, myx1, myx2, myx3, myx4, myx5))


############################################################
println("\nTest support_activeset")

supp = [1, 2, 4]
x0 = rand(r)
@time myxs1, err1 = support_activeset(AtA, Atb, btb, supp)
@time myxs2, err2 = support_activeset(AtA, Atb, btb, supp, x0)
@time myxs3, err3 = support_activeset(AtA, Atb, btb, supp, sumtoone=true)

display(hcat(realx, myxs1, myxs2, myxs3))
display([err1 err2 err3])

############################################################
println("\nTest matrix activeset")

W = rand(m,r)
trueH = rand(r,n)
X = W*trueH
WtX = W'*X
WtW = W'*W
@time myH1 = matrixactiveset(X, W)
@time myH2 = matrixactiveset(WtX, WtW, inputgram=true)
@time myH3 = matrixactiveset(X, W, rand(r, n))
display([norm(trueH - curH) for curH in [myH1, myH2, myH3]])
