# Contributing to GIANT.jl

Thank you for contributing to GIANT.jl.
Here are a few guidelines.

## Getting started

- Create a Gitlab.com account if you don't have one
- Give a star to the project :star:
- Send your Gitlab.com username to [Nicolas](http://nicolasnadisic.xyz/) and ask to be added to the GIANT Team
- Configure your git client and your [SSH key](https://docs.gitlab.com/ee/user/ssh.html#generate-an-ssh-key-pair). If using VS Code, you may be interested by [this Gitlab extension](https://marketplace.visualstudio.com/items?itemName=GitLab.gitlab-workflow).

## Git workflow

We use different branches to have a smoother development workflow.
Every new feature must be developed on an independent branch.

- Clone the repository (in a terminal, `git clone git@gitlab.com:giant-team/giant.jl.git`)
- Create a branch (`git checkout -b <my-feature-name>`)
- Do your work and commit/push regularly your progress
    - `git commit -m "I did this and that"`
    - `git push origin <my-feature-name>`
- Do not forget proper testing
- Once finished, [open a merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) to integrate your feature branch in `main`

## Julia workflow

Once cloned, enter the project's folder and start Julia.

Type `]` to enter Pkg mode, then type `activate .` and then `instantiate` to install the necessary packages.
Then type backspace to leave Pkg mode.

You should write a dedicated test script for your feature, in `test/test_<my-feature-name>.jl`, to help during development and to ensure the correctness and performance of your code. Take example from the existing files.

## Julia style guide

GIANT.jl does not really follow a style guide, but we try to make it consistent.
You should try to imitate the style of the existing code.

A few tips:
- All your code (except tests) should be in functions, you should never use global variables
- Try to respect the NMF notations from the README.md
- Name matrix variables with capital letters and vector variables with lower-case letters
- Name functions in lower case, if needed with underscores as word separators
- For functions with many parameters, write one parameter per line
- Specify types as much as possible, being as general as you need and as specific as you can
- Write informative comments 
- Consistency is the key

To write faster code, you should have a look at [performance tips](https://docs.julialang.org/en/v1/manual/performance-tips/).
